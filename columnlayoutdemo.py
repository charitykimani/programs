# This program is taken from 
# https://docs.bokeh.org/en/latest/docs/user_guide/layout.html
# with modifications to output an HTML file
from bokeh.io import output_file, show
from bokeh.layouts import column
from bokeh.plotting import figure
# the next two lines are needed to export to HTML
from bokeh.resources import CDN
from bokeh.embed import file_html


x = list(range(11))
y0 = x
y1 = [10 - i for i in x]
y2 = [abs(i - 5) for i in x]

# create three plots
s1 = figure(plot_width=250, plot_height=250, background_fill_color="#fafafa")
s1.circle(x, y0, size=12, color="#53777a", alpha=0.8)

s2 = figure(plot_width=250, plot_height=250, background_fill_color="#fafafa")
s2.triangle(x, y1, size=12, color="#c02942", alpha=0.8)

s3 = figure(plot_width=250, plot_height=250, background_fill_color="#fafafa")
s3.square(x, y2, size=12, color="#d95b43", alpha=0.8)

## Comment this out and replace it with output to an HTML page
# put the results in a column and show
#show(column(s1, s2, s3))

# Make an HTML page with the plot

html = file_html(column(s1,s2,s3), CDN, "Plot with column layout")
with open('index.html', 'w') as f:
    f.writelines(html)

